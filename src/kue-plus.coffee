async = require 'async'
module.exports = Kue = require 'kue'

Kue::runJob = (name, info, options, callback) ->
	if typeof options is 'function'
		callback = options
		options = {}

	if typeof info is 'function'
		callback = info
		options = {}
		info = {}


	options.priority ?= 0
	options.attempts ?= 3
	options.removeCompleted ?= true

	job = @create(name, info)
	job.priority options.priority
	job.attempts options.attempts

	job.on 'failed', () ->
		console.log 'FAILED', job, arguments
		callback null
	job.on 'progress', options.progress or () ->
		console.log 'PROGRESS', job, arguments
		null
	job.on 'complete', () =>
		job.get 'result', (err, data) ->
			try
				if err then throw err
				callback null, JSON.parse data
			catch e
				callback e

			if options.removeCompleted
				# have to remove it in this manner, or it doesnt get deleted properly.
				Kue.Job.get job.id, (err, job) ->
					job.remove () -> null

	job.save()
	return job

Kue::processJobs = (name, callback) ->
	@process name, (job, next) =>
		callback job, (err, result) =>
			if err
				return next err

			job.set 'result', JSON.stringify(result), () ->
				next()

Kue::register = (id, properties, callback) ->
	@properties = properties
	@properties.id = id
	@id = id

	setInterval (() =>
		@registerUpdate()
	), 1000

	listen = Kue.redis.createClient()
	listen.subscribe 'client-' + @id
	listen.on 'message', (err, message) =>
		if err.match /__keyevent@.__:expired/
			client = message.substring 12
			@emit 'event.client.exit', {client: id: client}, client
			return

		{message, client} = JSON.parse message
		@emit 'message', message, client

		if message.task?
			@emit "task.#{message.task}", message, client

		if message.event?
			@emit "event.#{message.event}", message, client

	key = ['kue-clients', @id].join ':'
	@client.set key, (JSON.stringify @properties), () =>
		@messageClients {event: 'client.start', client: @properties}, () =>
			@registerUpdate()
			callback and callback properties


	# if we can support it with redis, handle disconnected clients
	# this functionality is supported from 2.7.101 onwards
	@client.config 'set', 'notify-keyspace-events', 'xKE', (err) ->
		if not err
			console.info "Redis >= v2.7.101, client dropouts will be detected"
			listen.subscribe '__keyevent@0__:expired'


Kue::registerUpdate = () ->
	key = ['kue-clients', @id].join ':'
	@client.expire key, 3

Kue::getClients = (conditions, callback) ->
	if typeof conditions is 'function'
		callback = conditions
		conditions = {}

	@client.keys 'kue-clients:*', (err, keys) =>
		clients = {}
		iter = (key, next) =>
			@client.get key, (err, properties) ->
				clients[key.substring(12)] = JSON.parse properties
				next err

		async.each keys, iter, () ->
			limited = {}
			for id, client of clients
				client.id = id
				if (1 for k, v of conditions when client[k] isnt v).length is 0
					limited[id] = client
			callback null, limited

Kue::messageClients = (message, conditions, callback) ->
	if typeof conditions is 'function'
		callback = conditions
		conditions = {}
	if typeof message is 'function'
		throw 'Kue::messageClients requires a message as the first parameter'

	info =
		client: @id,
		message: message
	info = JSON.stringify info
	@getClients conditions, (err, clients) =>
		fns = for client of clients
			do (client) => (next) =>
				@client.publish 'client-' + client, info, (err) ->
					next err, client


		async.parallel fns, (err) =>
			callback and callback err, clients
