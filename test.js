kue = require('./index')

j1 = kue.createQueue()
j1.register('random', {type: 'testing'})

j1.on('message', function(message) {
	console.log('J1', message)
})

setTimeout(function() {
	// j1.getClients({type: 'testing'}, function() {
	// 	console.log('Clients', arguments)
	// })

	j2 = kue.createQueue()
	j2.register('foo', {type: 'foobar'})
	j2.on('message', function(message) {
		console.log('J2', message);
	})

}, 500)

setTimeout(function() {
	j1.messageClients({test: 'message'}, function() {
		console.log('Messaged', arguments);
	})
}, 1000)
